﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _Blog_11.Models
{
    public class Comment
    {
        public int ID { set; get; }
        
        [Required(ErrorMessage="Trường bắt buộc nhập.")]
        [MinLength(20, ErrorMessage="Độ dài ngắn nhất là 20 ký tự.")]
        public String Body { set; get; }

        [Required(ErrorMessage="Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return ((DateTime.Now - DateCreated).Minutes);
            }
        }

        public int Gio
        { 
            get
            {
                return (DateTime.Now - DateCreated).Hours;
            }
        }

        public int Giay
        {
            get
            {
                return (DateTime.Now - DateCreated).Seconds;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }


    }
}