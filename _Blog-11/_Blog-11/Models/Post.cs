﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _Blog_11.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage="Trường bắt buộc nhập.")]
        [MaxLength(500,ErrorMessage="Độ dài tối đa 500 ký tự.")]
        [MinLength(20,ErrorMessage="Độ dài tối thiểu là 20 ký tự.")]
        public String Title { set; get; }

        [Required(ErrorMessage = "Trường bắt buộc nhập.")]
        [MinLength(50,ErrorMessage="Độ dài tối thiểu là 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Trường bắt buộc nhập.")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
    }
}