﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _Blog_11.Models
{
    public class Tag
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Trường bắt buộc nhập.")]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
             
    }
}