namespace _Blog_11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "UserProfile_UserId", c => c.Int());
            AddForeignKey("dbo.Comments", "UserProfile_UserId", "dbo.UserProfile", "UserId");
            CreateIndex("dbo.Comments", "UserProfile_UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Comments", "UserProfile_UserId", "dbo.UserProfile");
            DropColumn("dbo.Comments", "UserProfile_UserId");
        }
    }
}
