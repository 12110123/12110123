﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class TestResultController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TestResult/

        public ActionResult Index()
        {
            return View(db.TestResults.ToList());
        }

        //
        // GET: /TestResult/Details/5

        public ActionResult Details(int id = 0)
        {
            TestResult testresult = db.TestResults.Find(id);
            if (testresult == null)
            {
                return HttpNotFound();
            }
            return View(testresult);
        }

        //
        // GET: /TestResult/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TestResult/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TestResult testresult)
        {
            if (ModelState.IsValid)
            {
                db.TestResults.Add(testresult);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(testresult);
        }

        //
        // GET: /TestResult/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TestResult testresult = db.TestResults.Find(id);
            if (testresult == null)
            {
                return HttpNotFound();
            }
            return View(testresult);
        }

        //
        // POST: /TestResult/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TestResult testresult)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testresult).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testresult);
        }

        //
        // GET: /TestResult/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TestResult testresult = db.TestResults.Find(id);
            if (testresult == null)
            {
                return HttpNotFound();
            }
            return View(testresult);
        }

        //
        // POST: /TestResult/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TestResult testresult = db.TestResults.Find(id);
            db.TestResults.Remove(testresult);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}