﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class ExerciseAnswerDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ExerciseAnswerDetail/

        public ActionResult Index()
        {
            var exerciseanswerdetails = db.ExerciseAnswerDetails.Include(e => e.ExerciseDetail);
            return View(exerciseanswerdetails.ToList());
        }

        //
        // GET: /ExerciseAnswerDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            ExerciseAnswerDetail exerciseanswerdetail = db.ExerciseAnswerDetails.Find(id);
            if (exerciseanswerdetail == null)
            {
                return HttpNotFound();
            }
            return View(exerciseanswerdetail);
        }

        //
        // GET: /ExerciseAnswerDetail/Create

        public ActionResult Create()
        {
            ViewBag.ExerciseDetailId = new SelectList(db.ExerciseDetails, "Id", "QuestionTitle");
            return View();
        }

        //
        // POST: /ExerciseAnswerDetail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExerciseAnswerDetail exerciseanswerdetail)
        {
            if (ModelState.IsValid)
            {
                db.ExerciseAnswerDetails.Add(exerciseanswerdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ExerciseDetailId = new SelectList(db.ExerciseDetails, "Id", "QuestionTitle", exerciseanswerdetail.ExerciseDetailId);
            return View(exerciseanswerdetail);
        }

        //
        // GET: /ExerciseAnswerDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ExerciseAnswerDetail exerciseanswerdetail = db.ExerciseAnswerDetails.Find(id);
            if (exerciseanswerdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExerciseDetailId = new SelectList(db.ExerciseDetails, "Id", "QuestionTitle", exerciseanswerdetail.ExerciseDetailId);
            return View(exerciseanswerdetail);
        }

        //
        // POST: /ExerciseAnswerDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExerciseAnswerDetail exerciseanswerdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exerciseanswerdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ExerciseDetailId = new SelectList(db.ExerciseDetails, "Id", "QuestionTitle", exerciseanswerdetail.ExerciseDetailId);
            return View(exerciseanswerdetail);
        }

        //
        // GET: /ExerciseAnswerDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ExerciseAnswerDetail exerciseanswerdetail = db.ExerciseAnswerDetails.Find(id);
            if (exerciseanswerdetail == null)
            {
                return HttpNotFound();
            }
            return View(exerciseanswerdetail);
        }

        //
        // POST: /ExerciseAnswerDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ExerciseAnswerDetail exerciseanswerdetail = db.ExerciseAnswerDetails.Find(id);
            db.ExerciseAnswerDetails.Remove(exerciseanswerdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}