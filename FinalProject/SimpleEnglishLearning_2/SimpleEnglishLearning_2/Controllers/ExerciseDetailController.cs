﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class ExerciseDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ExerciseDetail/

        public ActionResult Index()
        {
            var exercisedetails = db.ExerciseDetails.Include(e => e.SubjectDetail);
            return View(exercisedetails.ToList());
        }

        //
        // GET: /ExerciseDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            ExerciseDetail exercisedetail = db.ExerciseDetails.Find(id);
            if (exercisedetail == null)
            {
                return HttpNotFound();
            }
            return View(exercisedetail);
        }

        //
        // GET: /ExerciseDetail/Create

        public ActionResult Create()
        {
            ViewBag.SubjectDetailId = new SelectList(db.SubjectDetails, "Id", "UnitTitle");
            return View();
        }

        //
        // POST: /ExerciseDetail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExerciseDetail exercisedetail)
        {
            if (ModelState.IsValid)
            {
                db.ExerciseDetails.Add(exercisedetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectDetailId = new SelectList(db.SubjectDetails, "Id", "UnitTitle", exercisedetail.SubjectDetailId);
            return View(exercisedetail);
        }

        //
        // GET: /ExerciseDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ExerciseDetail exercisedetail = db.ExerciseDetails.Find(id);
            if (exercisedetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubjectDetailId = new SelectList(db.SubjectDetails, "Id", "UnitTitle", exercisedetail.SubjectDetailId);
            return View(exercisedetail);
        }

        //
        // POST: /ExerciseDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExerciseDetail exercisedetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exercisedetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectDetailId = new SelectList(db.SubjectDetails, "Id", "UnitTitle", exercisedetail.SubjectDetailId);
            return View(exercisedetail);
        }

        //
        // GET: /ExerciseDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ExerciseDetail exercisedetail = db.ExerciseDetails.Find(id);
            if (exercisedetail == null)
            {
                return HttpNotFound();
            }
            return View(exercisedetail);
        }

        //
        // POST: /ExerciseDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ExerciseDetail exercisedetail = db.ExerciseDetails.Find(id);
            db.ExerciseDetails.Remove(exercisedetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}