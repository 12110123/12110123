﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class QuestionAnswerDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /QuestionAnswerDetail/

        public ActionResult Index()
        {
            var questionanswerdetails = db.QuestionAnswerDetails.Include(q => q.QuestionDetail);
            return View(questionanswerdetails.ToList());
        }

        //
        // GET: /QuestionAnswerDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            QuestionAnswerDetail questionanswerdetail = db.QuestionAnswerDetails.Find(id);
            if (questionanswerdetail == null)
            {
                return HttpNotFound();
            }
            return View(questionanswerdetail);
        }

        //
        // GET: /QuestionAnswerDetail/Create

        public ActionResult Create()
        {
            ViewBag.QuestionDetailId = new SelectList(db.QuestionDetails, "Id", "QuestionTitle");
            return View();
        }

        //
        // POST: /QuestionAnswerDetail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionAnswerDetail questionanswerdetail)
        {
            if (ModelState.IsValid)
            {
                db.QuestionAnswerDetails.Add(questionanswerdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.QuestionDetailId = new SelectList(db.QuestionDetails, "Id", "QuestionTitle", questionanswerdetail.QuestionDetailId);
            return View(questionanswerdetail);
        }

        //
        // GET: /QuestionAnswerDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuestionAnswerDetail questionanswerdetail = db.QuestionAnswerDetails.Find(id);
            if (questionanswerdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionDetailId = new SelectList(db.QuestionDetails, "Id", "QuestionTitle", questionanswerdetail.QuestionDetailId);
            return View(questionanswerdetail);
        }

        //
        // POST: /QuestionAnswerDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionAnswerDetail questionanswerdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(questionanswerdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionDetailId = new SelectList(db.QuestionDetails, "Id", "QuestionTitle", questionanswerdetail.QuestionDetailId);
            return View(questionanswerdetail);
        }

        //
        // GET: /QuestionAnswerDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuestionAnswerDetail questionanswerdetail = db.QuestionAnswerDetails.Find(id);
            if (questionanswerdetail == null)
            {
                return HttpNotFound();
            }
            return View(questionanswerdetail);
        }

        //
        // POST: /QuestionAnswerDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionAnswerDetail questionanswerdetail = db.QuestionAnswerDetails.Find(id);
            db.QuestionAnswerDetails.Remove(questionanswerdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}