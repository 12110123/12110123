﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class SubjectDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /SubjectDetail/

        public ActionResult Index()
        {
            var subjectdetails = db.SubjectDetails.Include(s => s.Subject);
            return View(subjectdetails.ToList());
        }

        //
        // GET: /SubjectDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            SubjectDetail subjectdetail = db.SubjectDetails.Find(id);
            if (subjectdetail == null)
            {
                return HttpNotFound();
            }
            return View(subjectdetail);
        }

        //
        // GET: /SubjectDetail/Create

        public ActionResult Create()
        {
            ViewBag.SubjectID = new SelectList(db.Subjects, "Id", "SubjectName");
            return View();
        }

        //
        // POST: /SubjectDetail/Create

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubjectDetail subjectdetail)
        {
            if (ModelState.IsValid)
            {
                db.SubjectDetails.Add(subjectdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectID = new SelectList(db.Subjects, "Id", "SubjectName", subjectdetail.SubjectID);
            return View(subjectdetail);
        }

        //
        // GET: /SubjectDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SubjectDetail subjectdetail = db.SubjectDetails.Find(id);
            if (subjectdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubjectID = new SelectList(db.Subjects, "Id", "SubjectName", subjectdetail.SubjectID);
            return View(subjectdetail);
        }

        //
        // POST: /SubjectDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubjectDetail subjectdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subjectdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectID = new SelectList(db.Subjects, "Id", "SubjectName", subjectdetail.SubjectID);
            return View(subjectdetail);
        }

        //
        // GET: /SubjectDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SubjectDetail subjectdetail = db.SubjectDetails.Find(id);
            if (subjectdetail == null)
            {
                return HttpNotFound();
            }
            return View(subjectdetail);
        }

        //
        // POST: /SubjectDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubjectDetail subjectdetail = db.SubjectDetails.Find(id);
            db.SubjectDetails.Remove(subjectdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}