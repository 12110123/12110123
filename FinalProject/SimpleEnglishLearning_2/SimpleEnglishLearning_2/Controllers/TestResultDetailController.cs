﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class TestResultDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TestResultDetail/

        public ActionResult Index()
        {
            var testresultdetails = db.TestResultDetails.Include(t => t.TestResult);
            return View(testresultdetails.ToList());
        }

        //
        // GET: /TestResultDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            TestResultDetail testresultdetail = db.TestResultDetails.Find(id);
            if (testresultdetail == null)
            {
                return HttpNotFound();
            }
            return View(testresultdetail);
        }

        //
        // GET: /TestResultDetail/Create

        public ActionResult Create()
        {
            ViewBag.TestResultId = new SelectList(db.TestResults, "Id", "Detail");
            return View();
        }

        //
        // POST: /TestResultDetail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TestResultDetail testresultdetail)
        {
            if (ModelState.IsValid)
            {
                db.TestResultDetails.Add(testresultdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TestResultId = new SelectList(db.TestResults, "Id", "Detail", testresultdetail.TestResultId);
            return View(testresultdetail);
        }

        //
        // GET: /TestResultDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TestResultDetail testresultdetail = db.TestResultDetails.Find(id);
            if (testresultdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.TestResultId = new SelectList(db.TestResults, "Id", "Detail", testresultdetail.TestResultId);
            return View(testresultdetail);
        }

        //
        // POST: /TestResultDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TestResultDetail testresultdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testresultdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TestResultId = new SelectList(db.TestResults, "Id", "Detail", testresultdetail.TestResultId);
            return View(testresultdetail);
        }

        //
        // GET: /TestResultDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TestResultDetail testresultdetail = db.TestResultDetails.Find(id);
            if (testresultdetail == null)
            {
                return HttpNotFound();
            }
            return View(testresultdetail);
        }

        //
        // POST: /TestResultDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TestResultDetail testresultdetail = db.TestResultDetails.Find(id);
            db.TestResultDetails.Remove(testresultdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}