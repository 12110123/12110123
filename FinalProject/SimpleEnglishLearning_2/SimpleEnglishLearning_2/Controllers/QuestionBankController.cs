﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class QuestionBankController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /QuestionBank/

        public ActionResult Index()
        {
            var questionbanks = db.QuestionBanks.Include(q => q.Subject);
            return View(questionbanks.ToList());
        }

        //
        // GET: /QuestionBank/Details/5

        public ActionResult Details(int id = 0)
        {
            QuestionBank questionbank = db.QuestionBanks.Find(id);
            if (questionbank == null)
            {
                return HttpNotFound();
            }
            return View(questionbank);
        }

        //
        // GET: /QuestionBank/Create

        public ActionResult Create()
        {
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName");
            return View();
        }

        //
        // POST: /QuestionBank/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionBank questionbank)
        {
            if (ModelState.IsValid)
            {
                db.QuestionBanks.Add(questionbank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", questionbank.SubjectId);
            return View(questionbank);
        }

        //
        // GET: /QuestionBank/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuestionBank questionbank = db.QuestionBanks.Find(id);
            if (questionbank == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", questionbank.SubjectId);
            return View(questionbank);
        }

        //
        // POST: /QuestionBank/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionBank questionbank)
        {
            if (ModelState.IsValid)
            {
                db.Entry(questionbank).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "SubjectName", questionbank.SubjectId);
            return View(questionbank);
        }

        //
        // GET: /QuestionBank/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuestionBank questionbank = db.QuestionBanks.Find(id);
            if (questionbank == null)
            {
                return HttpNotFound();
            }
            return View(questionbank);
        }

        //
        // POST: /QuestionBank/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionBank questionbank = db.QuestionBanks.Find(id);
            db.QuestionBanks.Remove(questionbank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}