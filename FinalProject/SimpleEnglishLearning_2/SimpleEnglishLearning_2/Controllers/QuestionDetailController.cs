﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleEnglishLearning_2.Models;

namespace SimpleEnglishLearning_2.Controllers
{
    public class QuestionDetailController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /QuestionDetail/

        public ActionResult Index()
        {
            var questiondetails = db.QuestionDetails.Include(q => q.QuestionBank);
            return View(questiondetails.ToList());
        }

        //
        // GET: /QuestionDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            QuestionDetail questiondetail = db.QuestionDetails.Find(id);
            if (questiondetail == null)
            {
                return HttpNotFound();
            }
            return View(questiondetail);
        }

        //
        // GET: /QuestionDetail/Create

        public ActionResult Create()
        {
            ViewBag.QuestionBankId = new SelectList(db.QuestionBanks, "Id", "TestCode");
            return View();
        }

        //
        // POST: /QuestionDetail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionDetail questiondetail)
        {
            if (ModelState.IsValid)
            {
                db.QuestionDetails.Add(questiondetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.QuestionBankId = new SelectList(db.QuestionBanks, "Id", "TestCode", questiondetail.QuestionBankId);
            return View(questiondetail);
        }

        //
        // GET: /QuestionDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuestionDetail questiondetail = db.QuestionDetails.Find(id);
            if (questiondetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionBankId = new SelectList(db.QuestionBanks, "Id", "TestCode", questiondetail.QuestionBankId);
            return View(questiondetail);
        }

        //
        // POST: /QuestionDetail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionDetail questiondetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(questiondetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionBankId = new SelectList(db.QuestionBanks, "Id", "TestCode", questiondetail.QuestionBankId);
            return View(questiondetail);
        }

        //
        // GET: /QuestionDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuestionDetail questiondetail = db.QuestionDetails.Find(id);
            if (questiondetail == null)
            {
                return HttpNotFound();
            }
            return View(questiondetail);
        }

        //
        // POST: /QuestionDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionDetail questiondetail = db.QuestionDetails.Find(id);
            db.QuestionDetails.Remove(questiondetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}