namespace SimpleEnglishLearning_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_1st : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        SubjectDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubjectDetails", t => t.SubjectDetailId, cascadeDelete: true)
                .Index(t => t.SubjectDetailId);
            
            CreateTable(
                "dbo.SubjectDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Unit = c.Int(nullable: false),
                        UnitTitle = c.String(),
                        Introduction = c.String(),
                        Content = c.String(),
                        UserProfileId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                        Subject_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.Subjects", t => t.Subject_Id)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.Subject_Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        SubjectDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubjectDetails", t => t.SubjectDetailId, cascadeDelete: true)
                .Index(t => t.SubjectDetailId);
            
            CreateTable(
                "dbo.Exercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseNumber = c.String(),
                        SubjectDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubjectDetails", t => t.SubjectDetailId, cascadeDelete: true)
                .Index(t => t.SubjectDetailId);
            
            CreateTable(
                "dbo.ExerciseDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionNumber = c.Int(nullable: false),
                        QuestionTitle = c.String(),
                        AnswerKey = c.String(),
                        SubjectDetailId = c.Int(nullable: false),
                        Exercise_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubjectDetails", t => t.SubjectDetailId, cascadeDelete: true)
                .ForeignKey("dbo.Exercises", t => t.Exercise_Id)
                .Index(t => t.SubjectDetailId)
                .Index(t => t.Exercise_Id);
            
            CreateTable(
                "dbo.ExerciseAnswerDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnswerDetailNumber = c.Int(nullable: false),
                        AnswerDetail = c.String(),
                        ExerciseDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExerciseDetails", t => t.ExerciseDetailId, cascadeDelete: true)
                .Index(t => t.ExerciseDetailId);
            
            CreateTable(
                "dbo.QuestionAnswerDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnswerDetailNumber = c.Int(nullable: false),
                        AnswerDetail = c.String(),
                        QuestionDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionDetails", t => t.QuestionDetailId, cascadeDelete: true)
                .Index(t => t.QuestionDetailId);
            
            CreateTable(
                "dbo.QuestionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionNumber = c.Int(nullable: false),
                        QuestionTitle = c.String(),
                        AnswerKey = c.String(),
                        QuestionBankId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionBanks", t => t.QuestionBankId, cascadeDelete: true)
                .Index(t => t.QuestionBankId);
            
            CreateTable(
                "dbo.QuestionBanks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TestCode = c.String(),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TestResults",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Detail = c.String(),
                        Score = c.Double(nullable: false),
                        DateCompleted = c.DateTime(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.TestResultDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionNumber = c.Int(nullable: false),
                        Answer = c.String(),
                        AnswerKey = c.String(),
                        TestResultId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestResults", t => t.TestResultId, cascadeDelete: true)
                .Index(t => t.TestResultId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TestResultDetails", new[] { "TestResultId" });
            DropIndex("dbo.TestResults", new[] { "UserProfile_UserId" });
            DropIndex("dbo.QuestionBanks", new[] { "SubjectId" });
            DropIndex("dbo.QuestionDetails", new[] { "QuestionBankId" });
            DropIndex("dbo.QuestionAnswerDetails", new[] { "QuestionDetailId" });
            DropIndex("dbo.ExerciseAnswerDetails", new[] { "ExerciseDetailId" });
            DropIndex("dbo.ExerciseDetails", new[] { "Exercise_Id" });
            DropIndex("dbo.ExerciseDetails", new[] { "SubjectDetailId" });
            DropIndex("dbo.Exercises", new[] { "SubjectDetailId" });
            DropIndex("dbo.Tags", new[] { "SubjectDetailId" });
            DropIndex("dbo.SubjectDetails", new[] { "Subject_Id" });
            DropIndex("dbo.SubjectDetails", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Comments", new[] { "SubjectDetailId" });
            DropForeignKey("dbo.TestResultDetails", "TestResultId", "dbo.TestResults");
            DropForeignKey("dbo.TestResults", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.QuestionBanks", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.QuestionDetails", "QuestionBankId", "dbo.QuestionBanks");
            DropForeignKey("dbo.QuestionAnswerDetails", "QuestionDetailId", "dbo.QuestionDetails");
            DropForeignKey("dbo.ExerciseAnswerDetails", "ExerciseDetailId", "dbo.ExerciseDetails");
            DropForeignKey("dbo.ExerciseDetails", "Exercise_Id", "dbo.Exercises");
            DropForeignKey("dbo.ExerciseDetails", "SubjectDetailId", "dbo.SubjectDetails");
            DropForeignKey("dbo.Exercises", "SubjectDetailId", "dbo.SubjectDetails");
            DropForeignKey("dbo.Tags", "SubjectDetailId", "dbo.SubjectDetails");
            DropForeignKey("dbo.SubjectDetails", "Subject_Id", "dbo.Subjects");
            DropForeignKey("dbo.SubjectDetails", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "SubjectDetailId", "dbo.SubjectDetails");
            DropTable("dbo.TestResultDetails");
            DropTable("dbo.TestResults");
            DropTable("dbo.Subjects");
            DropTable("dbo.QuestionBanks");
            DropTable("dbo.QuestionDetails");
            DropTable("dbo.QuestionAnswerDetails");
            DropTable("dbo.ExerciseAnswerDetails");
            DropTable("dbo.ExerciseDetails");
            DropTable("dbo.Exercises");
            DropTable("dbo.Tags");
            DropTable("dbo.SubjectDetails");
            DropTable("dbo.Comments");
            DropTable("dbo.UserProfile");
        }
    }
}
