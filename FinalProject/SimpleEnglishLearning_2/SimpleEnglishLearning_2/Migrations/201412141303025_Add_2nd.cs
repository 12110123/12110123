namespace SimpleEnglishLearning_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_2nd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Email", c => c.String());
            AddColumn("dbo.UserProfile", "Address", c => c.String());
            AddColumn("dbo.UserProfile", "PhoneNumber", c => c.String());
            AddColumn("dbo.UserProfile", "Job", c => c.String());
            AddColumn("dbo.UserProfile", "DateOfBirth", c => c.String());
            AddColumn("dbo.UserProfile", "Gender", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Gender");
            DropColumn("dbo.UserProfile", "DateOfBirth");
            DropColumn("dbo.UserProfile", "Job");
            DropColumn("dbo.UserProfile", "PhoneNumber");
            DropColumn("dbo.UserProfile", "Address");
            DropColumn("dbo.UserProfile", "Email");
        }
    }
}
