namespace SimpleEnglishLearning_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_3rd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubjectDetails", "Subject_Id", "dbo.Subjects");
            DropIndex("dbo.SubjectDetails", new[] { "Subject_Id" });
            RenameColumn(table: "dbo.SubjectDetails", name: "Subject_Id", newName: "SubjectID");
            AddForeignKey("dbo.SubjectDetails", "SubjectID", "dbo.Subjects", "Id", cascadeDelete: true);
            CreateIndex("dbo.SubjectDetails", "SubjectID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.SubjectDetails", new[] { "SubjectID" });
            DropForeignKey("dbo.SubjectDetails", "SubjectID", "dbo.Subjects");
            RenameColumn(table: "dbo.SubjectDetails", name: "SubjectID", newName: "Subject_Id");
            CreateIndex("dbo.SubjectDetails", "Subject_Id");
            AddForeignKey("dbo.SubjectDetails", "Subject_Id", "dbo.Subjects", "Id");
        }
    }
}
