﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class TestResult
    {
        public int Id { get; set; }
        public string Detail { get; set; }
        public double Score { get; set; }
        public DateTime DateCompleted { get; set; }

        public int UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<TestResultDetail> TestResultDetails { get; set; }
    }
}