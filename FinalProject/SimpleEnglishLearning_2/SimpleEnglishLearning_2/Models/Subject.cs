﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class Subject
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }

        public virtual ICollection<QuestionBank> QuestionBank { get; set; }
        public virtual ICollection<SubjectDetail> SubjectDetail { get; set; }
    }
}