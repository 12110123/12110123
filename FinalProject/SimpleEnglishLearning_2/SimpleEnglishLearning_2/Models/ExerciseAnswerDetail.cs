﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class ExerciseAnswerDetail
    {
        public int Id { get; set; }
        public int AnswerDetailNumber { get; set; }
        public string AnswerDetail { get; set; }

        public int ExerciseDetailId { get; set; }
        public virtual ExerciseDetail ExerciseDetail { get; set; }
    }
}