﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class QuestionDetail
    {
        public int Id { get; set; }

        public int QuestionNumber { get; set; }
        public string QuestionTitle { get; set; }
        public string AnswerKey { get; set; }

        public virtual ICollection<QuestionAnswerDetail> QuestionAnswerDetails { get; set; }

        public int QuestionBankId { get; set; }
        public virtual QuestionBank QuestionBank { get; set; }
    }
}