﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class Exercise
    {
        public int Id { get; set; }
        public string ExerciseNumber { get; set; }

        public int SubjectDetailId { get; set; }
        public virtual SubjectDetail SubjectDetail { get; set; }

        public virtual ICollection<ExerciseDetail> ExerciseDetails { get; set; }
    }
}