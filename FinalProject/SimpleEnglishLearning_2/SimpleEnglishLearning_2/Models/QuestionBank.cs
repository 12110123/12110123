﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class QuestionBank
    {
        public int Id { get; set; }
        public string TestCode { get; set; }

        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public virtual ICollection<QuestionDetail> QuestionDetails { get; set; }
    }
}