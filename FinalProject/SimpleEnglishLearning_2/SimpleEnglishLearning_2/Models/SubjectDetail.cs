﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class SubjectDetail
    {
        public int Id { get; set; }
        public int Unit { get; set; }
        public string UnitTitle { get; set; }
        public string Introduction { get; set; }
        public string Content { get; set; }

        public int UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public int SubjectID { get; set; }
        public virtual Subject Subject { get; set; }
    }
}