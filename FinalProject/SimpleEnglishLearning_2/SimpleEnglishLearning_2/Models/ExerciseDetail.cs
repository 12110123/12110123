﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class ExerciseDetail
    {
        public int Id { get; set; }
        public int QuestionNumber { get; set; }
        public string QuestionTitle { get; set; }
        public string AnswerKey { get; set; }

        public virtual ICollection<ExerciseAnswerDetail> ExerciseAnswerDetails { get; set; }

        public int SubjectDetailId { get; set; }
        public virtual SubjectDetail SubjectDetail { get; set; }
    }
}