﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class QuestionAnswerDetail
    {
        public int Id { get; set; }
        public int AnswerDetailNumber { get; set; }
        public string AnswerDetail { get; set; }

        public int QuestionDetailId { get; set; }
        public virtual QuestionDetail QuestionDetail { get; set; }
    }
}