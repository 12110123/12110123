﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class TestResultDetail
    {
        public int Id { get; set; }
        public int QuestionNumber { get; set; }
        public string Answer { get; set; }
        public string AnswerKey { get; set; }

        public int TestResultId { get; set; }
        public virtual TestResult TestResult { get; set; }
    }
}