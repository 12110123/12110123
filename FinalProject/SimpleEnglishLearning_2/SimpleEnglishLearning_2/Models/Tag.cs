﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleEnglishLearning_2.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public int SubjectDetailId { get; set; }
        public virtual SubjectDetail SubjectDetail { get; set; }
    }
}