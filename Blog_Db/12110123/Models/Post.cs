﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110123.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        public String Title { set; get; }

        [StringLength(500,ErrorMessage="Số lượng ký tự trong khoảng 50 -> 500",MinimumLength=50)]
        public String Body { set; get; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

        public int AccountID { set; get; }
        public Account Account { set; get; }
    }
}