﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110123.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [DataType(DataType.Password)]
        public String Password { set; get; }

        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }

        [StringLength(100,ErrorMessage="100 ký tự thôi bạn ơi",MinimumLength=0)]
        public String FirstName { set; get; }

        [StringLength(100, ErrorMessage = "100 ký tự thôi bạn ơi", MinimumLength = 0)]
        public String LastName { set; get; }

        public virtual ICollection<Post> Posts { set; get; }


    }
}