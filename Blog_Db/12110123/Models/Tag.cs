﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110123.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [StringLength(100, ErrorMessage="Số lượng ký tự từ 10 đến 100 nhé.",MinimumLength=10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}